package cool.domsgames.minigametemplate.enums;

public enum GameState {
    PREGAME,
    COUNTDOWN,
    STARTED,
    ENDED
}
