package cool.domsgames.minigametemplate.listeners;

import cool.domsgames.minigametemplate.GameManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public record PlayerJoinListener(GameManager gameManager) implements Listener {

    @EventHandler
    public void playerJoin(PlayerJoinEvent e) {
        final Player player = e.getPlayer();

        if (gameManager.setPlayerInGame(player)) return;
        if (gameManager.setPlayerSpectating(player)) return;

        e.getPlayer().kickPlayer("An unknown error occurred while joining the game.");
    }
}
